﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidPrinciples
{
    public class Designer : Employee
    {
        public Designer(float efficiencyCoefficient) : base(efficiencyCoefficient)
        {
        }

        public override void Work(int difficultyRating)
        {
            base.Work(difficultyRating);
            Console.WriteLine("Discussing design with clients");
            Console.WriteLine("Preparing demo design");
            Console.WriteLine("Showing demo for clients");
        }

        public override string ToString()
        {
            return "Designer: " + Name + " " + Age + " " + this.efficiencyCoefficient;
        }
    }
}
