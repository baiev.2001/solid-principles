﻿
namespace SolidPrinciples
{
    //Example of dependency invertion and single responsibility principle
    public class EmployeeService
    {
        private readonly ILogger logger;
        private readonly IStorage<Employee> storage;

        public EmployeeService(ILogger logger, IStorage<Employee> storage)
        {
            this.logger = logger;
            this.storage = storage;
        }
        public void Create(Employee employee)
        {
            logger.Log("Creating an instance of employee");
            storage.Add(employee);
            logger.Log("Employee successfulyy created");
        }

        public void Delete(Employee employee)
        {
            logger.Log("Deleting a employee");
            storage.Delete(employee);
        }

        public Employee Get(int id)
        {
            logger.Log("Reading an instace of employee from id " + id);
            return storage.Get(id);
        }
    }
}
