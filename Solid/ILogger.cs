﻿namespace SolidPrinciples
{
    //Interface of logger
    // Represents open/closed interface segregation and single responsibilitiy principles . 
    public interface ILogger
    {
        void Log(string text);
    }
}
