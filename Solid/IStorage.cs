﻿namespace SolidPrinciples
{
    // Interface of storage
    // Represents open/closed interface segregation and single responsibilitiy principles . 
    public interface IStorage<T>
    {
        void Add(T item);

        void Delete(T item);

        void Delete(int id);

        void Clear();

        T Get(int index);

    }
}
