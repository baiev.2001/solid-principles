﻿using System;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;

namespace SolidPrinciples
{
    //File implementation of storage. Writes data to file 'storage.txt' in the directory of program
    public class FileStorage<T> : IStorage<T>
    {
        private String FilePath { get; set; }
        private JavaScriptSerializer serializer;
        public FileStorage()
        {
            serializer = new JavaScriptSerializer();
            this.FilePath = Directory.GetCurrentDirectory() + "\\storage.txt";
            if (!File.Exists(FilePath))
            {
                File.Create(FilePath);
            }
        }

        public void Add(T item)
        {
            using (System.IO.StreamWriter file =
           new System.IO.StreamWriter(FilePath))
            {
                file.WriteLine(serializer.Serialize(item));
            }
        }

        public void Clear()
        {
            File.Create(FilePath);
        }

        public void Delete(T item)
        {
            string lineToFind = serializer.Serialize(item);
            string tempFile = Path.GetTempFileName();
            using (var sr = new StreamReader(FilePath))
            using (var sw = new StreamWriter(tempFile))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if (!lineToFind.Equals(line))
                    {
                        sw.WriteLine(line);
                    }
                }
            }
            File.Delete(FilePath);
            File.Move(tempFile, FilePath);
        }

        public void Delete(int id)
        {
            string tempFile = Path.GetTempFileName();
            using (var sr = new StreamReader(FilePath))
            using (var sw = new StreamWriter(tempFile))
            {
                string line;
                int i = 0;
                while ((line = sr.ReadLine()) != null)
                {
                    if(i++ != id)
                    {
                        sw.WriteLine(line);
                    }
                }
            }
            File.Delete(FilePath);
            File.Move(tempFile, FilePath);
        }

        public T Get(int index)
        {
            string line = File.ReadLines(FilePath).Skip(index - 1).First();
            return (T)serializer.Deserialize(line, typeof(T));
        }
    }
}
