﻿using System.Collections.Generic;

namespace SolidPrinciples
{
    // Implementation of storage, which uses List for keeping data.
    public class CollectionStorage<T> : IStorage<T>
    {
        private List<T> arr = new List<T>();
        public void Add(T item)
        {
            this.arr.Add(item);
        }

        public void Clear()
        {
            this.arr.Clear();
        }

        public void Delete(int id)
        {
            this.arr.RemoveAt(id);
        }

        public void Delete(T item)
        {
            this.arr.Remove(item);
        }

        public T Get(int index)
        {
            return this.arr[index];
        }
    }
}
