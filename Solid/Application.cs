﻿using System;

namespace SolidPrinciples
{
    public class Application
    {
        public static void Main(String[] args)
        {
            var logger = new FileLogger();
            var storage = new FileStorage<Employee>();
            EmployeeService personService = new EmployeeService(logger, storage);
            personService.Create(new Designer(0.5F) { Name = "John", Age = 25});
            Employee person = personService.Get(0);
            logger.Log(person.ToString());
        }
    }
}
