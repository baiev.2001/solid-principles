﻿using System;
using System.IO;

namespace SolidPrinciples
{
    // File implementation of logger. Writes information to file 'log.txt' in the directory of program
    public class FileLogger : ILogger
    {
        private String FilePath { get; set; }
        public FileLogger()
        {
            this.FilePath = Directory.GetCurrentDirectory() + "\\log.txt";
            if (!File.Exists(FilePath))
            {
                File.Create(FilePath);
            }
        }
        public void Log(string text)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(FilePath))
            {
                file.WriteLine(text);
            }
        }
    }
}
