﻿using System;

namespace SolidPrinciples
{
    public class Employee
    {
        protected float efficiencyCoefficient = 0;
        protected int age;
        public int Age
        {
            get => this.age;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException();
                }
                this.age = value;
            }
        }

        public string Name { get; set; }

        public Employee()
        {

        }

        public Employee(float efficiencyCoefficient)
        {
            this.efficiencyCoefficient = efficiencyCoefficient;
        }

        public override string ToString()
        {
            return "Employee: " + Name + " " + Age + " " + this.efficiencyCoefficient; ;
        }
        
        public virtual void Work(int difficultyRating)
        {
            Console.WriteLine($"I'll need for this task {this.CalculateWorkHours(difficultyRating)} hours");
        }
        protected virtual float CalculateWorkHours(int difficultyRating)
        {
            if (difficultyRating > 8 || difficultyRating < 1)
            {
                throw new ArgumentException();
            }
            float res = difficultyRating / efficiencyCoefficient;
            if (res > 8)
            {
                throw new Exception("This task is too hard for the employee");
            }
            return res;
        }
    }
}
