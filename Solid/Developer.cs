﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolidPrinciples
{
    class Developer: Employee
    {
        public Developer(float efficiencyCoefficient) : base(efficiencyCoefficient)
        {
        }

        public override void Work(int difficultyRating)
        {
            base.Work(difficultyRating);
            Console.WriteLine("Learning task requirements");
            Console.WriteLine("Writing realization");
            Console.WriteLine("Sending task for review");
        }

        public override string ToString()
        {
            return "Developer: " + Name + " " + Age + " " + this.efficiencyCoefficient;
        }
    }
}
