# SOLID principles lab created by Baiev Viktor
Solid folder contains example of
* Single responsibilitity principle
* Open close principle
* Interface segregation principle
* Dependency invertion principle

LiskovSubstitution folder contains example of liskov substitution principle